package com.wordpress.allrightsunreserved.kryptkicker2;

/**
 * The KryptKicker2 class is here to host the void main() method and get things
 * started with the DecryptionEngine
 * @author fsheikh
 *
 */
public class KryptKicker2 {

	public static void main(String[] args) {
		if (args.length == 0)
		{
			System.out.println("A path to the encrypted file must be provied");
			return;
		}
		
		DecryptionEngine codeCracker = new DecryptionEngine(args[0]);
		codeCracker.run();
		
		return;
	}

}
