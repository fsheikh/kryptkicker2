package com.wordpress.allrightsunreserved.kryptkicker2;

import java.io.*;
import java.util.*;

/**
 * This class provides a decryption engine for encrypted text as defined in the KryptKickerII problem
 * http://www.programming-challenges.com/pg.php?page=downloadproblem&format=html&probid=110304
 * @author fsheikh
 *
 */
public class DecryptionEngine implements Runnable {
	
	private static final String knownPhrase = new String("the quick brown fox jumps over the lazy dog");
	
	private String filePath;
	
	DecryptionEngine(String filePath)
	{
		this.filePath = filePath;
	}
	
	public void run() {
		
		//Each ArrayList<String> in the outer ArrayList will contain a separate encrypted case
		ArrayList<ArrayList<String>> encryptedCases = new ArrayList<ArrayList<String>>();
		readEncryptedFile(filePath, encryptedCases);
		
		for (ArrayList<String> encryptedCase : encryptedCases)
		{
			decrypt(encryptedCase);
		}
	}

	/**
	 * Populates the passed in String ArrayList with the encrypted text contained in a file at
	 * the passed in path
	 * 
	 * @param: path: This is the path of the encrypted text file
	 * @param: encryptedLines: This is the String ArrayList that will get populated with the encrypted text
	 */
	public static void readEncryptedFile(String path, ArrayList<ArrayList<String>> encyrptedCases)
	{
	    try {
	    	BufferedReader br = new BufferedReader(new FileReader(path));
	        String line = br.readLine(); 
	       
	        //This will contain a number defining the number of test cases in the file
	        int totalCases = Integer.parseInt(line.trim());
	        

	        while (line != null) {
	        	line = br.readLine();
	        	
	        	if (line.trim().length() == 0) //A blank line ... skip it
	        	{
	        		line = br.readLine();
	        	}
	        	
	        	ArrayList<String> encryptedCase = new ArrayList<String>();
	        	while (line != null && line.trim().length() > 0)
	        	{
	        		encryptedCase.add(line);
	        		line = br.readLine();
	        	}
	        	encyrptedCases.add(encryptedCase);
	        	
	        	//Check if we already got all the encrypted cases or hit the end of the file
	        	if (encyrptedCases.size() == totalCases || line == null)
	        		break;
	        }
	        
	        br.close();
	    } catch (Exception e)
	    {
	    	System.out.println("There was an error getting the encrypted data! Please make sure that the given file path is correct.");
	    }
	}
	

	/**
	 * Takes an encrypted case represented by an ArrayList<String>
	 * and outputs the decrypted text
	 * @param encryptedCase
	 */
	private static void decrypt(ArrayList<String> encryptedCase) {
		
		HashMap<String, String> keyMapping = new HashMap<String, String>();
		
		for (String encryptedLine: encryptedCase)
		{
			if (encryptedLine.length() == knownPhrase.length())
			{
				retrieveKeyMapping(encryptedLine, keyMapping);
				if (keyMapping.size() > 0)
					break;
			}
		}
		
		if (keyMapping.size() == 0)
		{
			System.out.println("No Solution \n");
			return;
		}
		
		for (String encryptedLine : encryptedCase)
		{
			for (int index = 0; index < encryptedLine.length(); index++)
			{
				String encryptedLineChar = encryptedLine.substring(index, index+1);
				System.out.print(keyMapping.get(encryptedLineChar));
			}
			System.out.print("\n");
		}
		System.out.println("");
	}

	/**
	 * Retrieves the key mapping of encrypted alphabet to the decrypted alphabet if one exists
	 * @param encryptedLine: This is the line of encrypted text
	 * @param keyMapping: This is the HashMap that will contain the encrypted to decrypted alphabet mapping
	 */
	private static void retrieveKeyMapping(String encryptedLine,
			HashMap<String, String> keyMapping) {
		
		for (int index = 0;  index < encryptedLine.length(); index++)
		{
			String encryptedLineChar = encryptedLine.substring(index, index+1);
			String knownPhraseChar = knownPhrase.substring(index, index+1);
			
			//The encrypted line cannot be the known phrase because a space is not matching up
			if (knownPhraseChar.equals(" ") && !encryptedLineChar.equals(" "))
			{
				keyMapping.clear();
				return;
			}
			
			if (keyMapping.containsKey(encryptedLineChar))
			{
				//The encrypted line cannot be the known phrase because each letter is mapped to a unique letter
				if (!keyMapping.get(encryptedLineChar).equals(knownPhraseChar))
				{
					keyMapping.clear();
					return;
				}
			}
			
			keyMapping.put(encryptedLineChar, knownPhraseChar);
		}
	}

}
